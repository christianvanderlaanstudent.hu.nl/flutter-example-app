import 'package:flutter/material.dart';
import 'package:udp/udp.dart';
import 'dart:io';

void main() async {
  // creates a UDP instance and binds it to the first available network
  // interface on port 65000.
  var sender = await UDP.bind(Endpoint.any(port: const Port(65000)));

  // send a simple string to a broadcast endpoint on port 65001.
  var dataLength = await sender.send(
      'Hello World!'.codeUnits, Endpoint.broadcast(port: const Port(65001)));

  stdout.write('$dataLength bytes sent.');

  // creates a new UDP instance and binds it to the local address and the port
  // 65002.
  var receiver = await UDP.bind(Endpoint.loopback(port: const Port(65002)));

  // receiving\listening
  receiver.asStream(timeout: const Duration(seconds: 20)).listen((datagram) {
    var str = String.fromCharCodes(datagram!.data);
    stdout.write(str);
  });

  // close the UDP instances and their sockets.
  sender.close();
  receiver.close();
}

class UDPsend extends StatelessWidget {
  const UDPsend({Key? key}) : super(key: key);

  static const String _title = 'Flutter Code Sample';

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: _title,
      home: Scaffold(
          appBar: AppBar(
            title: const Text('UDP Page'),
            automaticallyImplyLeading: true,
            leading: IconButton(
              onPressed: () {
                Navigator.of(context).pop();
              },
              icon: const Icon(Icons.arrow_back),
            ),
          ),
          body: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              ElevatedButton(
                onPressed: () {},
                child: const Text("UDP"),
              )
            ],
          )),
    );
  }
}
